package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.epica.pruebagrabilitymovie.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

import models.Serie;

/**
 * Created by Epica on 24/3/2017.
 */

public class SerieAdapter extends RecyclerView.Adapter<SerieViewHolder>{

    private final Context context;
    ArrayList<Serie> series_lista;

    public SerieAdapter(Context context){

        this.context=context;

    }

    public void addData(List<Serie> serie){
        series_lista = new ArrayList<>();
        series_lista.addAll(serie);
        this.notifyDataSetChanged();
    }

    @Override
    public SerieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_media, parent, false);
        return new SerieViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SerieViewHolder holder, int position) {
        Serie serie = series_lista.get(position);
        Picasso.with(context)
                .load(context.getResources().getString(R.string.base_image_url)+serie.getPoster_path())
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.logo)
                .into(holder.getImg_backdrop());
        holder.getDetail_title().setText("Nombre: "+serie.getName());
        holder.getDetail_release().setText("Transmitido desde: "+serie.getFirst_air_date());
        holder.getDetail_adult().setText("Lenguaje Original: "+serie.getOriginal_language());
        holder.getDetail_overview().setText("Resumen: "+serie.getOverview());
        holder.getDetail_original_lengua().setText("Nombre Original: "+serie.getOriginal_name());
        holder.getRating().setRating(serie.getVote_average());

    }

    @Override
    public int getItemCount() {
        return (null != series_lista ? series_lista.size() : 0);
    }
}
