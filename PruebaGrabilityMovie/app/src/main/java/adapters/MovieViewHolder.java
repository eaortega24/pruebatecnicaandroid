package adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.epica.pruebagrabilitymovie.R;

/**
 * Created by Epica on 23/3/2017.
 */

public class MovieViewHolder  extends RecyclerView.ViewHolder {
    private RatingBar rating;
    private TextView  detail_title;
    private TextView  detail_release;
    private TextView  detail_adult;
    private TextView  detail_overview;
    private TextView  detail_original_lengua;
    private ImageView img_backdrop;

    public MovieViewHolder(View itemView) {
        super(itemView);
        rating = (RatingBar) itemView.findViewById(R.id.rating);
        detail_adult = (TextView) itemView.findViewById(R.id.detail_adult);
        detail_title = (TextView) itemView.findViewById(R.id.detail_title);
        detail_release = (TextView) itemView.findViewById(R.id.detail_release);
        detail_overview = (TextView) itemView.findViewById(R.id.detail_overview);
        detail_original_lengua = (TextView) itemView.findViewById(R.id.detail_original_lengua);
        img_backdrop = (ImageView) itemView.findViewById(R.id.img_backdrop);

    }

    public RatingBar getRating() {
        return rating;
    }

    public TextView getDetail_title() {
        return detail_title;
    }

    public TextView getDetail_release() {
        return detail_release;
    }

    public TextView getDetail_adult() {
        return detail_adult;
    }

    public TextView getDetail_overview() {
        return detail_overview;
    }

    public TextView getDetail_original_lengua() {
        return detail_original_lengua;
    }

    public ImageView getImg_backdrop() {
        return img_backdrop;
    }
}
