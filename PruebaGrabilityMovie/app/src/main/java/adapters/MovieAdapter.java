package adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.epica.pruebagrabilitymovie.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import models.Movie;

/**
 * Created by Epica on 22/3/2017.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieViewHolder>{
    private final Context context;
    ArrayList<Movie> peliculas;

    public MovieAdapter(Context context){
        this.context=context;
    }

    public void addData(List<Movie> peli){
        peliculas = new ArrayList<>();
        peliculas.addAll(peli);
        this.notifyDataSetChanged();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_media, parent, false);
        return new MovieViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Movie movie = peliculas.get(position);
        Picasso.with(context)
                .load(context.getResources().getString(R.string.base_image_url)+movie.getPoster_path())
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.logo)
                .into(holder.getImg_backdrop());
            String adulto="Adulto: ";
            holder.getDetail_adult().setText(movie.isAdult()?adulto+"Yes":adulto+"No");
            holder.getDetail_original_lengua().setText("Idioma: "+movie.getOriginal_language());
            holder.getDetail_overview().setText("Resumen: "+movie.getOverview());
            holder.getDetail_release().setText("Lanzamiento: "+movie.getRelease_date());
            holder.getRating().setRating(movie.getVote_average());
            holder.getDetail_title().setText("Nombre: "+movie.getTitle());

    }

    @Override
    public int getItemCount() {
        return (null != peliculas ? peliculas.size() : 0);
    }
}
