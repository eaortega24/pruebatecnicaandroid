package utils;

import android.app.Application;

/**
 * Created by Epica on 21/3/2017.
 */

public class RxApplication extends Application {

    private NetworkService networkService;
    @Override
    public void onCreate() {
        super.onCreate();

        networkService = new NetworkService();

    }

    public NetworkService getNetworkService(){
        return networkService;
    }


}

