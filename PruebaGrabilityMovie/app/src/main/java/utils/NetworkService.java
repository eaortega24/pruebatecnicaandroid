package utils;

import android.support.v4.util.LruCache;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import models.TmdbMovie;
import models.TmdbSeries;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Epica on 21/3/2017.
 */

public class NetworkService {
    private static String baseUrl ="http://api.themoviedb.org/3/";
    private NetworkAPI networkAPI;
    private OkHttpClient okHttpClient;
    private LruCache<Class<?>, Observable<?>> apiObservables;

    public NetworkService(){
        this(baseUrl);
    }

    public NetworkService(String baseUrl){
        okHttpClient = buildClient();
        apiObservables = new LruCache<>(10);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    public NetworkAPI getAPI(){
        return  networkAPI;
    }

    public OkHttpClient buildClient(){

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        });

        return  builder.build();
    }

    public void clearCache(){
        apiObservables.evictAll();
    }

    public Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache){

        Observable<?> preparedObservable = null;

        if(useCache)
            preparedObservable = apiObservables.get(clazz);

        if(preparedObservable!=null)
            return preparedObservable;

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if(cacheObservable){
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }


        return preparedObservable;
    }

    public interface NetworkAPI {

        @GET("movie/popular")
        Observable<TmdbMovie> getMoviePopular(@Query("api_key")String api_key, @Query("page") int page);

        @GET("tv/popular")
        Observable<TmdbSeries> getSeriesPopular(@Query("api_key")String api_key, @Query("page") int page);

        @GET("tv/top_rated")
        Observable<TmdbSeries> getTopRatedSerie(@Query("api_key")String api_key, @Query("page") int page);


        @GET("movie/top_rated")
        Observable<TmdbMovie> getTopRatedMovie(@Query("api_key")String api_key, @Query("page") int page);

        @GET("movie/upcoming")
        Observable<TmdbMovie> getUpcomingMovie(@Query("api_key")String api_key, @Query("page") int page);

    }

}
