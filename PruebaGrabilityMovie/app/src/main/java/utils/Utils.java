package utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.epica.pruebagrabilitymovie.R;

/**
 * Created by Epica on 22/3/2017.
 */

public class Utils {
    public static void show_toast(Context ctx, String message, int p_duration) {
        Toast toast = new Toast(ctx);
        toast = Toast.makeText(ctx, message, p_duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static Dialog get_progress_dialog(Context p_context) {
        try {
            Dialog dialog = new Dialog(p_context, R.style.DialogCustom);
            dialog.setCancelable(false);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.addContentView(new ProgressBar(p_context), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT));

            return dialog;
        } catch (Exception err) {
            return null;
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
