package presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.ArrayList;

import database.DBOperations;
import interfaces.IFragmentMovieView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import models.Movie;
import models.TmdbMovie;
import utils.NetworkService;
import utils.Utils;

/**
 * Created by Epica on 22/3/2017.
 */

public class MoviePresenter {
    private IFragmentMovieView iteractor;
    private NetworkService service;
    private DBOperations dbOperations;
    private Context contexto;
    private String api_key;
    Fragment frg;
    private String pantalla;

    public MoviePresenter(Fragment frg, NetworkService service, String api_key,String pantalla){
        this.frg = frg;
        this.pantalla = pantalla;
        this.contexto=frg.getActivity();
        this.service=service;
        this.api_key = api_key;
        iteractor = ((IFragmentMovieView) frg);
        dbOperations =DBOperations.getInstance(frg.getContext());
        iteractor.setNameScreen(procesarNombre());
        setMovieFromDB();
    }

    public void setMovieFromDB(){
        iteractor.showDialog();
        Observable.just(dbOperations.getAllMovie(pantalla))
                .subscribe(new Observer<ArrayList<Movie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<Movie> value) {
                        ArrayList<Movie> v = value;
                        iteractor.setData(value);

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        iteractor.dissmissDialog();
                    }
                });
    }

    public void DownloadRxMoviePopular(int page){
        if(Utils.isOnline(contexto)){
            iteractor.showDialog();
            service.clearCache();
            Observable<TmdbMovie> movieObservable = getObservableToScreen(page);

            movieObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<TmdbMovie>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(TmdbMovie value) {

                            dbOperations.insertMovie(value);

                            iteractor.dissmissDialog();

                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onComplete() {
                            iteractor.setData(dbOperations.getAllMovie(pantalla));
                        }
                    });
        }
    }

    public void loadMore(int page) {

        if(Utils.isOnline(contexto)) {
            this.DownloadRxMoviePopular(page);
        }
        else {
            setMovieFromDB();
            Utils.show_toast(contexto,"Se encuentra sin conexion de red", Toast.LENGTH_SHORT);
        }

    }

private Observable<TmdbMovie> getObservableToScreen(int page){
    if(pantalla.equals("vote_average")){
        return (Observable<TmdbMovie>)
                service.getPreparedObservable(service.getAPI().getTopRatedMovie(api_key,page), TmdbMovie.class,true,true);
    }else if(pantalla.equals("popularity")){
        return (Observable<TmdbMovie>)
                service.getPreparedObservable(service.getAPI().getMoviePopular(api_key,page), TmdbMovie.class,true,true);
    }else{
        return (Observable<TmdbMovie>)
                service.getPreparedObservable(service.getAPI().getUpcomingMovie(api_key,page), TmdbMovie.class,true,true);
    }

}

    private String procesarNombre() {
        if(pantalla.equals("vote_average")){
            return "Most Rated Movie";
        }else if(pantalla.equals("popularity")){
            return "Popular Movies";
        }else{
            return "Upcoming";
        }
    }
}
