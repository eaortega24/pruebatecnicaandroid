package presenter;

import android.content.Context;

import com.example.epica.pruebagrabilitymovie.R;

import database.DBOperations;
import interfaces.ISplashView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import models.TmdbMovie;
import models.TmdbSeries;
import utils.NetworkService;
import utils.Utils;

/**
 * Created by Epica on 21/3/2017.
 */

public class SplashPresenter  {

    private int numRequest = 0;
    private NetworkService service;

    private DBOperations dbOperations;
    private ISplashView iteractor;
    private Context contexto;
    public SplashPresenter(Context context, NetworkService service){
        contexto=context;
        this.service=service;
        iteractor = (ISplashView)context;
        dbOperations =DBOperations.getInstance(context);
    }

    public void loadRxMovie(String api_key, int page){
        if(Utils.isOnline(contexto)){
                Observable<TmdbMovie> movieObservable = (Observable<TmdbMovie>)
                        service.getPreparedObservable(service.getAPI().getMoviePopular(api_key,page), TmdbMovie.class,true,true);

                movieObservable.subscribeOn(Schedulers.io())// .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .subscribe(new Observer<TmdbMovie>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onNext(TmdbMovie value) {
                                dbOperations.dropTable("peliculas");
                                //iteractor.passListMovie(value.getResults());

                                dbOperations.insertMovie(value);
                                numRequest++;
                            }

                            @Override
                            public void onError(Throwable e) {
                                numRequest--;
                                iteractor.showError("Error: " + e.getCause().toString());
                            }

                            @Override
                            public void onComplete() {
                                iteractor.changeScreen(numRequest);
                            }
                        });

        }
        else{
            if(dbOperations.isEmptyTable("peliculas")){
                numRequest--;
                iteractor.showError("Error: " + contexto.getResources().getString(R.string.error_tabla));
            }
            else{
                numRequest++;
                iteractor.passListMovie(dbOperations.getAllMovie("popularity"));
                iteractor.changeScreen(numRequest);
            }
        }

    }
    public void loadRxSeries(String api_key,int page){
        if(Utils.isOnline(contexto)){


        Observable<TmdbSeries> serieObservable = (Observable<TmdbSeries>)
                service.getPreparedObservable(service.getAPI().getSeriesPopular(api_key,page), TmdbSeries.class,true,true);
        serieObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<TmdbSeries>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(TmdbSeries value) {
                        dbOperations.dropTable("series");
                       // iteractor.passListSerie(value.getResults());
                        dbOperations.insertSerie(value);
                        numRequest++;
                    }

                    @Override
                    public void onError(Throwable e) {
                        numRequest--;
                        iteractor.showError("Error: " + e.getCause().toString());
                    }

                    @Override
                    public void onComplete() {
                        iteractor.changeScreen(numRequest);
                    }
                });



        }else{
            if(dbOperations.isEmptyTable("series")){
                numRequest--;
                iteractor.showError("Error: " + contexto.getResources().getString(R.string.error_tabla));
            }
            else{
                numRequest++;
                iteractor.changeScreen(numRequest);
            }
        }

    }



}
