package presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.ArrayList;

import database.DBOperations;
import interfaces.IFragmentMovieView;
import interfaces.IFragmentSerie;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import models.Serie;
import models.TmdbSeries;
import utils.NetworkService;
import utils.Utils;

/**
 * Created by Epica on 21/3/2017.
 */

public class SeriePresenter {
    private IFragmentSerie iteractor;
    private NetworkService service;
    private DBOperations dbOperations;
    private Context contexto;
    private String api_key;
    Fragment frg;
    private String pantalla;

    public SeriePresenter(Fragment frg, NetworkService service, String api_key,String pantalla){
        this.frg = frg;
        this.pantalla = pantalla;
        this.contexto=frg.getActivity();
        this.service=service;
        this.api_key = api_key;
        iteractor = ((IFragmentSerie) frg);
        dbOperations =DBOperations.getInstance(frg.getContext());
        iteractor.setNameScreen(procesarNombre());
        setSerieByDb();
    }


    private void setSerieByDb() {
        iteractor.showDialog();
        Observable.just(dbOperations.getAllSerie(pantalla))
                .subscribe(new Observer<ArrayList<Serie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<Serie> value) {
                        ArrayList<Serie> v = value;
                        iteractor.setSeries(value);

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        iteractor.dissmissDialog();
                    }
                });
    }


    public void DownloadRxSerie(int page){
        if(Utils.isOnline(contexto)){
            iteractor.showDialog();
            service.clearCache();
            Observable<TmdbSeries> movieObservable = getObservableToScreen(page);

            movieObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<TmdbSeries>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(TmdbSeries value) {

                            dbOperations.insertSerie(value);

                            iteractor.dissmissDialog();

                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onComplete() {
                            iteractor.setSeries(dbOperations.getAllSerie(pantalla));
                        }
                    });
        }
    }


    private Observable<TmdbSeries> getObservableToScreen(int page){
        if(pantalla.equals("vote_average")){
            return (Observable<TmdbSeries>)
                    service.getPreparedObservable(service.getAPI().getTopRatedSerie(api_key,page), TmdbSeries.class,true,true);
        }else{
            return (Observable<TmdbSeries>)
                    service.getPreparedObservable(service.getAPI().getSeriesPopular(api_key,page), TmdbSeries.class,true,true);
        }

    }

    public void loadMore(int page) {

        if(Utils.isOnline(contexto)) {
            this.DownloadRxSerie(page);
        }
        else {
            setSerieByDb();
            Utils.show_toast(contexto,"Se encuentra sin conexion de red", Toast.LENGTH_SHORT);
        }

    }

    private String procesarNombre() {
        if(pantalla.equals("vote_average")){
            return "Most Rated Tv Show";
        }else {
            return "Popular Tv Show";
        }
    }
}


/*
* public class MoviePresenter {


    public MoviePresenter(Fragment frg, NetworkService service, String api_key,String pantalla){
        this.frg = frg;
        this.pantalla = pantalla;
        this.contexto=frg.getActivity();
        this.service=service;
        this.api_key = api_key;
        iteractor = ((IFragmentMovieView) frg);
        dbOperations =DBOperations.getInstance(frg.getContext());
        iteractor.setNameScreen(procesarNombre());
        setMovieFromDB();
    }

    public void setMovieFromDB(){
        iteractor.showDialog();
        Observable.just(dbOperations.getAllMovie(pantalla))
                .subscribe(new Observer<ArrayList<Movie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<Movie> value) {
                        //frg.addData(value);
                        ArrayList<Movie> v = value;
                        iteractor.setData(value);

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        iteractor.dissmissDialog();
                    }
                });
    }

    public void DownloadRxMoviePopular(int page){
        if(Utils.isOnline(contexto)){
            iteractor.showDialog();
            service.clearCache();
            Observable<TmdbMovie> movieObservable = getObservableToScreen(page);

            movieObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<TmdbMovie>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(TmdbMovie value) {

                            dbOperations.insertMovie(value);

                            iteractor.dissmissDialog();

                        }

                        @Override
                        public void onError(Throwable e) {
                        }

                        @Override
                        public void onComplete() {
                            iteractor.setData(dbOperations.getAllMovie(pantalla));
                        }
                    });
        }
    }

    public void loadMore(int page) {

        if(Utils.isOnline(contexto)) {
            this.DownloadRxMoviePopular(page);
        }
        else {
            setMovieFromDB();
            Utils.show_toast(contexto,"Se encuentra sin conexion de red", Toast.LENGTH_SHORT);
        }

    }

private Observable<TmdbMovie> getObservableToScreen(int page){
    if(pantalla.equals("vote_average")){
        return (Observable<TmdbMovie>)
                service.getPreparedObservable(service.getAPI().getTopRatedMovie(api_key,page), TmdbMovie.class,true,true);
    }else if(pantalla.equals("popularity")){
        return (Observable<TmdbMovie>)
                service.getPreparedObservable(service.getAPI().getMoviePopular(api_key,page), TmdbMovie.class,true,true);
    }else{
        return (Observable<TmdbMovie>)
                service.getPreparedObservable(service.getAPI().getUpcomingMovie(api_key,page), TmdbMovie.class,true,true);
    }

}

    private String procesarNombre() {
        if(pantalla.equals("vote_average")){
            return "Most Rated Movie";
        }else if(pantalla.equals("popularity")){
            return "Popular Movies";
        }else{
            return "Upcoming";
        }
    }
}

*
* */