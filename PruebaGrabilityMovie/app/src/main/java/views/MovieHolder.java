package views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Epica on 23/3/2017.
 */

public class MovieHolder extends RecyclerView.ViewHolder {
    public ImageView poster;

    public MovieHolder(View itemView) {
        super(itemView);
    }
}
