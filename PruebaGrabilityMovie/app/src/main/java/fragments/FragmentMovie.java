package fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.epica.pruebagrabilitymovie.R;

import java.util.List;

import adapters.MovieAdapter;
import interfaces.FragmentComunicator;
import interfaces.IFragmentMovieView;
import models.Movie;
import presenter.MoviePresenter;
import utils.NetworkService;
import utils.Utils;

/**
 * Created by Epica on 20/3/2017.
 */

public class FragmentMovie extends Fragment implements IFragmentMovieView {

    private View v;
    Dialog pd;
    private FragmentComunicator comunicacion;
    private String screen;
    private FloatingActionButton load_more;
    private MoviePresenter presenter;
    private NetworkService service;
    private int page = 1;
    private RecyclerView recicler;
    private MovieAdapter adapter;
    public static FragmentMovie _construct(String pantalla){
        FragmentMovie fragment = new FragmentMovie();
        Bundle argumentos = new Bundle();
        argumentos.putString("pantalla",pantalla);
        fragment.setArguments(argumentos);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle arguments = getArguments();
        if (arguments != null) {
            screen = (String) arguments.getString("pantalla");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_list, container, false);
        pd = Utils.get_progress_dialog(this.getActivity());
        adapter = new MovieAdapter(getActivity()/*,lista_peliculas*/);
        service = new NetworkService();

        page=page+1;
        comunicacion = (FragmentComunicator) getContext();


        load_more = (FloatingActionButton) v.findViewById(R.id.fab);
        recicler=(RecyclerView)v.findViewById(R.id.recicler);
        recicler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false));


        recicler.setAdapter(adapter);
        presenter = new MoviePresenter(this,service,getResources().getString(R.string.api_key),screen);

        load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.loadMore(page);
                page=page+1;
            }
        });


        return v;
    }


    @Override
    public void onPause() {
        super.onPause();
        if(pd.isShowing())
        pd.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(pd.isShowing())
            pd.dismiss();
    }

    @Override
    public void showDialog() {
        pd.show();
    }

    @Override
    public void dissmissDialog() {
        pd.dismiss();
    }

    @Override
    public void setData(List<Movie> movies) {
        adapter.addData(movies);
    }

    @Override
    public void setNameScreen(String name) {
        comunicacion.nombreFragment(name);
    }


}
