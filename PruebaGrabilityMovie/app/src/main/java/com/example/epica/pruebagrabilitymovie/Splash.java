package com.example.epica.pruebagrabilitymovie;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import interfaces.ISplashView;
import models.Movie;
import models.Serie;
import presenter.SplashPresenter;
import utils.NetworkService;
import utils.Utils;

public class Splash extends AppCompatActivity implements ISplashView{
    private NetworkService service;
    SplashPresenter presenter;
    Dialog pd;
    List<Movie> mov;
    List<Serie> ser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        service = new NetworkService();
        presenter=new SplashPresenter(this,service);
        pd = Utils.get_progress_dialog(this);
        pd.show();
        presenter.loadRxMovie(getResources().getString(R.string.api_key),1);
        presenter.loadRxSeries(getResources().getString(R.string.api_key),1);
    }


    @Override
    public void changeScreen(int numRequestSuccess) {
        if(numRequestSuccess==2){
            Intent intent = new Intent(Splash.this, MainActivity.class);
            startActivity(intent);
            pd.dismiss();
            finish();

        }
    }

    @Override
    public void showError(String error) {
        pd.dismiss();
        Utils.show_toast(this,error, Toast.LENGTH_LONG);
    }

    @Override
    public void passListMovie(List<Movie> movies) {
        mov = movies;
    }

    @Override
    public void passListSerie(List<Serie> serie) {
        ser=serie;
    }
}
