package com.example.epica.pruebagrabilitymovie;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import fragments.FragmentMovie;
import fragments.FragmentTv;
import interfaces.FragmentComunicator;

/**
 * Created by Epica on 20/3/2017.
 */

public class MainActivity extends AppCompatActivity implements FragmentComunicator {
    private Toolbar appbar;
    private DrawerLayout drawerLayout;
    private NavigationView navView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame,
                        FragmentMovie._construct("popularity"))
                .commit();
        appbar = (Toolbar)findViewById(R.id.appbar);
        setSupportActionBar(appbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        navView = (NavigationView)findViewById(R.id.navview);
        navView.setItemIconTintList(null);
        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {

                            case R.id.menu_peli_1:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame,
                        FragmentMovie._construct("popularity"))
                                        .addToBackStack(null)
                                        .commit();
                                break;
                            case R.id.menu_peli_2:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame,
                                                FragmentMovie._construct("vote_average"))
                                        .addToBackStack(null)
                                        .commit();
                                break;
                            case R.id.menu_peli_3:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame,
                                                FragmentMovie._construct("release_date"))
                                        .addToBackStack(null)
                                        .commit();

                                break;
                            case R.id.menu_tv_1:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame,
                                                FragmentTv._construct("popularity"))
                                        .addToBackStack(null)
                                        .commit();
                                break;
                            case R.id.menu_tv_2:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame,
                                                FragmentTv._construct("vote_average"))
                                        .addToBackStack(null)
                                        .commit();
                                break;
                            case R.id.menu_info:

                                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                                alertDialog.setTitle("Informacion");
                                alertDialog.setMessage(getResources().getString(R.string.informacion));
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.show();

                                break;
                        }



                        menuItem.setChecked(true);
                        getSupportActionBar().setTitle(menuItem.getTitle());
                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void nombreFragment(String nombre) {
        getSupportActionBar().setTitle(nombre);
    }

    @Override
    public void onBackPressed() {
        back();

    }
    private void back(){
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {

            getSupportFragmentManager().popBackStack();
        }
    }
}
