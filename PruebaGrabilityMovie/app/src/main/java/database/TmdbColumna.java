package database;

/**
 * Created by Epica on 20/3/2017.
 */

public class TmdbColumna {

    interface ColumnasTv{

        String BACKDROP_PATH="backdrop_path";
        String FIRST_AIR_DATE="first_air_date";
        String ID="id";
        String ORIGINALLANGUAGE="original_language";
        String ORIGINALNAME="original_name";
        String OVERVIEW="overview";
        String POPULARITY="popularity";
        String POSTER_PATH="poster_path";
        String VOTEAVERAGE = "vote_average";
        String NAME = "name";

    }

    interface ColumnasMovies{
        String ID="id";//
        String ADULT="adult";//
        String BACKDROP_PATH="backdrop_path";//
        String RELEASEDATE="release_date";//
        String ORIGINALLANGUAGE="original_language";//
        String OVERVIEW="overview";//
        String POSTER_PATH="poster_path";//
        String TITLE ="title";//
        String POPULARITY="popularity";//
        String VOTEAVERAGE ="vote_average";//
    }

}
