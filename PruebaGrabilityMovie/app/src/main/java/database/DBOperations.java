package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import models.Movie;
import models.Serie;
import models.TmdbMovie;
import models.TmdbSeries;


/**
 * Created by Epica on 20/3/2017.
 */

public class DBOperations {
    private static  DataBaseTmdb database;
    private static final int VERSION_ACTUAL = 2;
    private static final String NOMBRE_BASE_DATOS = "tmdb.db";
    private static DBOperations instancia = new DBOperations();

    public static DBOperations getInstance (Context context){
        if(database == null){
            database = new DataBaseTmdb(context,NOMBRE_BASE_DATOS,null,VERSION_ACTUAL);

        }
        return instancia;
    }

    public ArrayList<Movie> getAllMovie(String order) {
        SQLiteDatabase db = database.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s ORDER BY "+order+" DESC",
                DataBaseTmdb.Tablas.PELICULAS);

        Cursor cursor =db.rawQuery(sql, null);

        ArrayList<Movie> lista = new ArrayList<>();
        if(!isEmptyTable(DataBaseTmdb.Tablas.PELICULAS) && cursor!=null){
            Movie peli;
            cursor.moveToFirst();
            do {

                peli = new Movie();
                peli.setId(Long.valueOf(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.ID))));

                peli.setBackdrop_path(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.BACKDROP_PATH)));

                peli.setAdult(processint(cursor.getInt(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.ADULT))));

                peli.setRelease_date(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.RELEASEDATE)) ) ;

                peli.setOriginal_language(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.ORIGINALLANGUAGE))  );

                peli.setOverview( cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.OVERVIEW))   );

                peli.setPoster_path(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.POSTER_PATH))  );

                peli.setVote_average(Float.valueOf(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.VOTEAVERAGE))  ));

                peli.setTitle(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.TITLE))  );

                peli.setPopularity(Double.valueOf(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasMovies.POPULARITY))  ));


                lista.add(peli);
            }while (cursor.moveToNext());
            cursor.close();
        }

        return lista;
    }


    public ArrayList<Serie> getAllSerie(String order) {

        SQLiteDatabase db = database.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s ORDER BY "+order+" DESC",
                DataBaseTmdb.Tablas.SERIES);

        Cursor cursor =db.rawQuery(sql, null);

        ArrayList<Serie> lista = new ArrayList<>();
        if(!isEmptyTable(DataBaseTmdb.Tablas.SERIES) && cursor!=null){

            cursor.moveToFirst();
            Serie serie;

            do{
                serie = new Serie();

                serie.setPoster_path(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.POSTER_PATH)));

                serie.setPopularity(Double.valueOf(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.POPULARITY))  ));

                serie.setId(Long.valueOf(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.ID))));

                serie.setBackdrop_path(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.BACKDROP_PATH)));

                serie.setVote_average(Float.valueOf(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.VOTEAVERAGE))  ));

                serie.setOverview( cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.OVERVIEW))   );

                serie.setFirst_air_date(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.FIRST_AIR_DATE)));

                serie.setOriginal_language(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.ORIGINALLANGUAGE))  );

                serie.setName(  cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.NAME))  );

                serie.setOriginal_name(cursor.getString(cursor.getColumnIndex(TmdbColumna.ColumnasTv.ORIGINALNAME)));

                lista.add(serie);



            }while (cursor.moveToNext());
            cursor.close();
        }
        return lista;

    }



    private static boolean CheckExist(String TableName,String dbfield, String fieldValue) {

        SQLiteDatabase sqldb = database.getReadableDatabase();
        String Query = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


   public boolean isEmptyTable(String table){

            SQLiteDatabase db =  database.getReadableDatabase();
            String sql = String.format("SELECT count(*) FROM %s",
                            table);

            Cursor mcursor = db.rawQuery(sql, null);
            mcursor.moveToFirst();
            return (mcursor.getInt(0)<=0);

    }

   public void insertMovie(TmdbMovie tmdbMovie){

       Observable.just(tmdbMovie.getResults()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Movie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Movie> detail) {
                        SQLiteDatabase db = database.getWritableDatabase();
                        for (int i = 0; i < detail.size(); i++) {
                            if(!CheckExist(DataBaseTmdb.Tablas.PELICULAS,TmdbColumna.ColumnasMovies.ID,String.valueOf(detail.get(i).getId()))){

                                ContentValues valores = new ContentValues();
                                valores.put(TmdbColumna.ColumnasMovies.ID, String.valueOf(detail.get(i).getId()));
                                valores.put(TmdbColumna.ColumnasMovies.ADULT, processBoolean(detail.get(i).isAdult())); //*******
                                valores.put(TmdbColumna.ColumnasMovies.BACKDROP_PATH, detail.get(i).getBackdrop_path());
                                valores.put(TmdbColumna.ColumnasMovies.RELEASEDATE, detail.get(i).getRelease_date());
                                valores.put(TmdbColumna.ColumnasMovies.ORIGINALLANGUAGE, detail.get(i).getOriginal_language()); //*******
                                valores.put(TmdbColumna.ColumnasMovies.OVERVIEW, detail.get(i).getOverview());//*******
                                valores.put(TmdbColumna.ColumnasMovies.TITLE, detail.get(i).getTitle());
                                valores.put(TmdbColumna.ColumnasMovies.POSTER_PATH, detail.get(i).getPoster_path());
                                valores.put(TmdbColumna.ColumnasMovies.VOTEAVERAGE, String.valueOf(detail.get(i).getVote_average()));
                                valores.put(TmdbColumna.ColumnasMovies.POPULARITY, String.valueOf(detail.get(i).getPopularity()));

                                db.insertOrThrow(DataBaseTmdb.Tablas.PELICULAS, null, valores);
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }

   public void insertSerie(TmdbSeries serie){

       Observable.just(serie.getResults())
               .subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .unsubscribeOn(Schedulers.io())
               .subscribe(new Observer<List<Serie>>() {
                   @Override
                   public void onSubscribe(Disposable d) {

                   }

                   @Override
                   public void onNext(List<Serie> detail) {
                       for(int i = 0 ; i<detail.size(); i++){
                           SQLiteDatabase db =  database.getWritableDatabase();
                           if(!CheckExist(DataBaseTmdb.Tablas.SERIES,TmdbColumna.ColumnasTv.ID,String.valueOf(detail.get(i).getId()))){
                               ContentValues valores = new ContentValues();
                               valores.put(TmdbColumna.ColumnasTv.POSTER_PATH,detail.get(i).getPoster_path());
                               valores.put(TmdbColumna.ColumnasTv.POPULARITY,String.valueOf(detail.get(i).getPopularity()));
                               valores.put(TmdbColumna.ColumnasTv.BACKDROP_PATH,detail.get(i).getBackdrop_path());
                               valores.put(TmdbColumna.ColumnasTv.ID,detail.get(i).getId());
                               valores.put(TmdbColumna.ColumnasTv.VOTEAVERAGE,String.valueOf(detail.get(i).getVote_average()));
                               valores.put(TmdbColumna.ColumnasTv.OVERVIEW,detail.get(i).getOverview());//*******
                               valores.put(TmdbColumna.ColumnasTv.FIRST_AIR_DATE,detail.get(i).getFirst_air_date());
                               valores.put(TmdbColumna.ColumnasTv.ORIGINALLANGUAGE,detail.get(i).getOriginal_language());//*******
                               valores.put(TmdbColumna.ColumnasTv.NAME,detail.get(i).getName());
                               valores.put(TmdbColumna.ColumnasTv.ORIGINALNAME,detail.get(i).getOriginal_name());//*******
                               db.insertOrThrow(DataBaseTmdb.Tablas.SERIES, null, valores);
                           }
                       }
                   }

                   @Override
                   public void onError(Throwable e) {
                   }
                   @Override
                   public void onComplete() {

                   }
               });
   }

    public void dropTable(String table){
        SQLiteDatabase db =  database.getWritableDatabase();
        db.delete(table,null,null);
    }

    private int processBoolean(boolean flat){
        return flat?1:0;
    }
    private boolean processint(int flat){
        return (flat==1);
    }
}
