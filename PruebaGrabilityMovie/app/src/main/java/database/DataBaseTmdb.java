package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

/**
 * Created by Epica on 20/3/2017.
 */

public class DataBaseTmdb extends SQLiteOpenHelper {

    public DataBaseTmdb(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, version);
    }

    interface Tablas {
        String SERIES = "series";
        String PELICULAS = "peliculas";
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(String.format("CREATE TABLE %s (%s TEXT PRIMARY KEY," +
                "%s TEXT, %s TEXT," +
                "%s TEXT, %s TEXT," +
                "%s TEXT, %s TEXT," +
                "%s TEXT, %s TEXT," +
                "%s TEXT," +
                "%s INTEGER PRIMARY KEY AUTOINCREMENT)",
                Tablas.SERIES,TmdbColumna.ColumnasTv.ID,
                TmdbColumna.ColumnasTv.BACKDROP_PATH,TmdbColumna.ColumnasTv.FIRST_AIR_DATE,
                TmdbColumna.ColumnasTv.ORIGINALLANGUAGE,TmdbColumna.ColumnasTv.ORIGINALNAME,
                TmdbColumna.ColumnasTv.OVERVIEW,TmdbColumna.ColumnasTv.POPULARITY,
                TmdbColumna.ColumnasTv.POSTER_PATH,TmdbColumna.ColumnasTv.VOTEAVERAGE,
                TmdbColumna.ColumnasTv.NAME
        ));


        db.execSQL(String.format("CREATE TABLE %s (%s TEXT PRIMARY KEY," +
                        "%s INTEGER, " +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT," +
                        "%s TEXT)",
                        Tablas.PELICULAS,TmdbColumna.ColumnasMovies.ID,
                        TmdbColumna.ColumnasMovies.ADULT,
                        TmdbColumna.ColumnasMovies.BACKDROP_PATH,
                        TmdbColumna.ColumnasMovies.RELEASEDATE,
                        TmdbColumna.ColumnasMovies.ORIGINALLANGUAGE,
                        TmdbColumna.ColumnasMovies.OVERVIEW,
                        TmdbColumna.ColumnasMovies.POSTER_PATH,
                        TmdbColumna.ColumnasMovies.VOTEAVERAGE,
                        TmdbColumna.ColumnasMovies.POPULARITY,//Recuerda castearlo a double o float
                        TmdbColumna.ColumnasMovies.TITLE
        ));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + Tablas.PELICULAS);
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.SERIES);

            onCreate(db);
    }
}
