package interfaces;

import java.util.List;

import models.Movie;
import models.Serie;

/**
 * Created by Epica on 22/3/2017.
 */

public interface ISplashView {
    void changeScreen(int numRequestSuccess);
    void showError(String error);
    void passListMovie(List<Movie> movies);
    void passListSerie(List<Serie> serie);
}
