package interfaces;

import java.util.List;

import models.Movie;

/**
 * Created by Epica on 23/3/2017.
 */

public interface IFragmentMovieView {
    void showDialog();
    void dissmissDialog();
    void setData(List<Movie> movies);
    void setNameScreen(String name);
}
