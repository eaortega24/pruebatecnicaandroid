package interfaces;

import java.util.List;

import models.Serie;

/**
 * Created by Epica on 24/3/2017.
 */

public interface IFragmentSerie {
    void showDialog();
    void dissmissDialog();
    void setSeries(List<Serie> movies);
    void setNameScreen(String name);
}
